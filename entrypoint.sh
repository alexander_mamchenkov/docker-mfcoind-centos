#!/bin/bash


cd /root

if [ ! -d .MFCoin ]
then
    # get latest block data
    wget https://www.dropbox.com/s/f6a3465zudvltz9/latest.tar.gz?dl=1 -O latest.tar.gz
    tar -xzf latest.tar.gz
    rm latest.tar.gz

    # set defaults
    if [ -z $RPCUSER ]
    then
        RPCUSER=MFCoinrpc
    fi

    if [ -z $RPCPASS ]
    then
        RPCPASS=MFCoinrpcPass
    fi

    echo "rpcuser=$RPCUSER" >> /root/.MFCoin/MFCoin.conf
    echo "rpcpassword=$RPCPASS" >> /root/.MFCoin/MFCoin.conf
    echo "bind=0.0.0.0" >> /root/.MFCoin/MFCoin.conf

    if [ "$ALLOWRPC" ]
    then
        echo "rpcallowip=*" >> /root/.MFCoin/MFCoin.conf
    fi
fi

MFCoind -daemon -txindex && tail -f /root/.MFCoin/debug.log
