# docker-mfcoind-centos

MFCoind docker image based on CentOS 7

You can pass RPCUSER and RPCPASS env variables to change defaults:

User: MFCoinrpc
Pass: MFCoinrpcPass

You can also pass ALLOWRPC=1 to allow RPC from anywhere

Exposed ports:

* 22823: JSON-RPC 
* 22824: P2P Data

This image will also pull for latest available chain status from:
https://bitbucket.org/alexander_mamchenkov/mfcoind-data.git

DockerHub: https://hub.docker.com/r/mamchenkov/mfcoind-centos/
