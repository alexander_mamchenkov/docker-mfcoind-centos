FROM centos:7

LABEL org.label-schema.schema-version="1.0" \
    org.label-schema.name="MFCoind daemon" \
    org.label-schema.vendor="Alexander Mamchenkov" \
    org.label-schema.livence="MIT" \
    org.label-schema.build-data="20180702"

ENV RPCUSER MFCoinrpc
ENV RPCPASS MFCoinrpcPass

MAINTAINER Alexander Mamchenkov <mamtchenkov@gmail.com>

RUN yum install -y --setopt=tsflags=nodocs epel-release
RUN yum install -y --setopt=tsflags=nodocs autoconf \
    automake \
    boost-devel \
    gcc-c++ \
    git \
    libdb4-cxx \
    libdb4-cxx-devel \
    libevent-devel \
    libtool \
    openssl-devel \
    wget \
    miniupnpc-devel \
    patch \
    make \
    && yum clean all \
    && rm -rf /var/cache/yum

RUN cd /root/ \
    && wget https://www.openssl.org/source/openssl-1.0.1l.tar.gz \
    && tar zxvf openssl-1.0.1l.tar.gz \
    && rm -rf rm openssl-1.0.1l.tar.gz \
    && cd openssl-1.0.1l \
    && export CFLAGS="-fPIC" \
    && ./config --prefix=/opt/openssl shared enable-ec enable-ecdh enable-ecdsa \
    && make all \
    && make install \
    && cd ../ \
    && rm -rf openssl-1.0.1l \
    && git clone https://github.com/MFrcoin/MFCoin.git \
    && cd MFCoin/src \
    && make USE_UPNP=0 USE_IPV6=0 CXXFLAGS="-I/usr/include/libdb4 -L/usr/lib64/libdb4" -f makefile.unix \
    && mv MFCoind /usr/local/bin/ \
    && cd ../../ \
    && rm MFCoin/ -rf

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

EXPOSE 22823
EXPOSE 22824

CMD ["/usr/local/bin/entrypoint.sh"]
